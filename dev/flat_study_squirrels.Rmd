---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)



```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# get_message_fur_color

récupérer un message sur la couleur de la fourrure

```{r function-get_message_fur_color}
#' Récupérer un message sur la couleur de la fourrure
#' @param primary_fur_color char la couleur de la fourrure
#' @return un message
#' @export
get_message_fur_color <- function(primary_fur_color = "Cinnamon"){
    
message(glue::glue("We will focus on {primary_fur_color} squirrels"))
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  expect_error(get_message_fur_color(unavailable_object))
  expect_message(get_message_fur_color())
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "utiliser_les_ecureuils", overwrite = TRUE)
```
